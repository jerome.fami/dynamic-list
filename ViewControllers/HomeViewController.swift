//
//  HomeViewController.swift
//  Dynamic List
//
//  Created by Jerome Fami on 21/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import UIKit
import IGListKit

class HomeViewController: UIViewController {

    @IBOutlet weak var viewContainer: UIView!

    private var aSections = ASection()
    private var bSections = BSection()
    
    let collectionView: ListCollectionView = {
        let view = ListCollectionView(frame: CGRect.zero, listCollectionViewLayout: ListCollectionViewLayout(stickyHeaders: false, scrollDirection: .vertical, topContentInset: 10, stretchToEdge: true))
        view.backgroundColor = UIColor.black
        return view
    }()
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewContainer.addSubview(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        collectionView.frame = viewContainer.bounds
    }
    
    @IBAction func onPressListA(_ sender: Any) {
        let letters = "abcdefghijklmnopqrstuvwxyz"
        let random = String((0...5).map{ _ in letters.randomElement()! })
        
        aSections.contents.append(random)
        adapter.performUpdates(animated: true, completion: nil)
    }
    @IBAction func onPressListB(_ sender: Any) {
        let letters = "0123456789"
        let random = String((0...5).map{ _ in letters.randomElement()! })
        
        bSections.contents.append(random)
        adapter.performUpdates(animated: true, completion: nil)
    }
    
}

extension HomeViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        var items: [ListDiffable] = aSections.contents as [ListDiffable]
        items += bSections.contents as [ListDiffable]
        
        return items
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return SectionController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
    
}
