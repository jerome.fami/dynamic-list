//
//  ASectionController.swift
//  Dynamic List
//
//  Created by Jerome Fami on 21/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation
import IGListKit

class SectionController: ListSectionController {
    
    var entry: String!
    
    override init() {
        super.init()
        
        inset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 55)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: "ViewCell", bundle: nil, for: self, at: index) as! ViewCell
        cell.label.text = entry
        return cell
    }
    
    override func didUpdate(to object: Any) {
        entry = object as? String
    }
    
    override func didSelectItem(at index: Int) {
        
    }
    
}
