//
//  AViewCell.swift
//  Dynamic List
//
//  Created by Jerome Fami on 21/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import UIKit

class ViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
